FROM php:7.3-apache

WORKDIR /var/www/html

RUN apt-get update && apt-get install -y --fix-missing git unzip zip
RUN curl -sS https://getcomposer.org/installer | php \
  && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer

COPY . /var/www/html

RUN php /usr/local/bin/composer config --global discard-changes true && \
    /usr/local/bin/composer  --no-interaction install