service=price-terminal-service

install:
	@docker run --rm -v ${PWD}:/var/www/html $(service) composer update


docker-build:
	@docker build -t $(service) .

docker-build-dev:
	@docker build --build-arg XDEBUG_INSTALL=1 -t $(service) .

docker-run:
	@docker run -p 8080:80 --name=$(service) --rm \
	    -e XDEBUG_CONFIG="remote_host=172.17.0.1" \
	    -v ${PWD}:/var/www/html \
	    -d \
	    $(service)

test: docker-build
	@docker run --rm -i -P --name=$(service) \
		-v ${PWD}:/var/www/html -w /var/www/html $(service) \
		sh -c "php vendor/bin/phpunit --configuration phpunit.xml"