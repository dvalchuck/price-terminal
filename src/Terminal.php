<?php


namespace PriceTerminal;


use PriceTerminal\Product\Product;
use PriceTerminal\Product\ProductBuilder;
use PriceTerminal\Product\ProductsDecorator;

/**
 * Main terminal class.
 *
 * @package PriceTerminal
 */
class Terminal
{
	/**
	 * An array of Product objects.
	 *
	 * @var array
	 */
	protected $products = [];

	/**
	 * Product builder object.
	 *
	 * @var ProductBuilder
	 */
	private $productBuilder;

	/**
	 * Terminal constructor.
	 *
	 * @param array $productsConfiguration Terminal configuration.
	 */
	public function __construct(array $productsConfiguration)
	{
		$this->productBuilder = new ProductBuilder($productsConfiguration);
	}

	/**
	 * Adds products to the composite.
	 *
	 * @param string $id Product Id.
	 *
	 * @throws Exception\PriceTerminalException
	 */
	public function addProduct(string $id)
	{
		if (isset($this->products[$id])) {
			/** @var Product $product */
			$product = $this->products[$id];
			$product->addProductItem();
		}
		else {
			$this->products[$id] = $this->productBuilder->buildProduct($id)->getProduct();
		}
	}

	/**
	 * Add multiple products to the composite.
	 *
	 * @param array $productsIds Array of product Id's tp add.
	 *
	 * @throws Exception\PriceTerminalException
	 */
	public function addProducts(array $productsIds)
	{
		foreach ($productsIds as $Id) {
			$this->addProduct($Id);
		}
	}

	/**
	 * Calculates total price for all products based on product price configuration.
	 *
	 * @return float
	 */
	public function getTotal()
	{
		$total = 0;

		foreach ($this->products as $product) {
			$decorated = new ProductsDecorator($product);
			$total     += $decorated->getTotalPrice();
		}

		return $total;
	}

}