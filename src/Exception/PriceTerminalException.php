<?php


namespace PriceTerminal\Exception;

/**
 * Exception thrown if PriceTerminal filed to process input data.
 *
 * @package PriceTerminal\Exception
 */
class PriceTerminalException extends \Exception
{

}