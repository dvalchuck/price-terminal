<?php


namespace PriceTerminal\VolumePrice;

/**
 * Implements Chain of responsibility pattern to calculate total price including volume prices.
 *
 * @package PriceTerminal\VolumePrice
 */
class VolumePriceChain
{
	/**
	 * Current chain successor,
	 *
	 * @var VolumePriceChain
	 */
	private $successor;

	/**
	 * Total price for specific products.
	 *
	 * @var float
	 */
	private static $totalPrice = 0;

	/**
	 * Specific product's volume count.
	 *
	 * @var int
	 */
	private $volumeCount;

	/**
	 * Specific product's volume price.
	 *
	 * @var float
	 */
	private $volumePrice;

	/**
	 * Chain constructor.
	 *
	 * @param int   $volumeCount Specific product's volume count.
	 * @param float $volumePrice Specific product's volume price.
	 */
	public function __construct(int $volumeCount, float $volumePrice)
	{
		$this->volumeCount  = $volumeCount;
		$this->volumePrice  = $volumePrice;
		static::$totalPrice = 0;
	}

	/**
	 * Adds chain successor,
	 *
	 * @param VolumePriceChain $chain Next chain.
	 *
	 * @return $this
	 */
	public final function setNext(VolumePriceChain $chain)
	{
		$this->successor = $chain;

		return $this;
	}

	/**
	 * Calculates total price for specific product volume.
	 *
	 * @param int $productCount Current product count.
	 *
	 * @return float
	 */
	public function getTotalPrice(int $productCount)
	{
		if ($this->canHandle($productCount)) {
			self::increaseTotalPrice($productCount, $this->volumeCount, $this->volumePrice);
			$productCount = $productCount % $this->volumeCount;

		}

		if ($productCount && $this->successor) {
			$this->successor->getTotalPrice($productCount);
		}

		return static::$totalPrice;
	}

	/**
	 * Checks if current chain can be used.
	 *
	 * @param int $productCount Current product count.
	 *
	 * @return bool
	 */
	private function canHandle(int $productCount)
	{
		return $this->volumeCount <= $productCount;
	}

	/**
	 * Adds current chain price to total amount.
	 *
	 *
	 * @param int   $productCount Current product count.
	 * @param int   $volumeCount  Specific product's volume count.
	 * @param float $volumePrice  Specific product's volume price.
	 */
	private static function increaseTotalPrice(int $productCount, int $volumeCount, float $volumePrice)
	{
		$value = round($productCount / $volumeCount) * $volumePrice;

		static::$totalPrice += $value;
	}

}