<?php


namespace PriceTerminal\Product;


use PriceTerminal\VolumePrice\VolumePriceChain;

/**
 * Adds extra functional to the product object.
 *
 * @package PriceTerminal\Product
 */
class ProductsDecorator implements ProductInterface
{
	/**
	 * Simple product object.
	 *
	 * @var Product
	 */
	private $product;

	/**
	 * ProductsDecorator constructor.
	 *
	 * @param Product $product Simple product.
	 */
	public function __construct(Product $product)
	{
		$this->product = $product;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getId(): string
	{
		return $this->product->getId();
	}

	/**
	 * {@inheritDoc}
	 */
	public function getTotalPrice(): float
	{
		if (empty($this->product->getVolumePrices())) {
			return $this->product->getTotalPrice();
		}
		$chain = new VolumePriceChain(1, $this->product->getPrice());
		foreach ($this->product->getVolumePrices() as $volumeCount => $volumePrice) {
			$nextChain = new VolumePriceChain($volumeCount, $volumePrice);
			$chain     = $nextChain->setNext($chain);
		}

		return $chain->getTotalPrice($this->product->getProductItemsCount());
	}

}