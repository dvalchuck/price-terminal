<?php


namespace PriceTerminal\Product;

/**
 * Interface for simple product.
 *
 * @package PriceTerminal\Product
 */
interface ProductInterface
{

	/**
	 * Returns product ID.
	 *
	 * @return string
	 */
	public function getId(): string;

	/**
	 * Returns total price for all products in group.
	 *
	 * @return float
	 */
	public function getTotalPrice(): float;

}