<?php

namespace PriceTerminal\Product;

/**
 * Simple product class.
 *
 * @package PriceTerminal\Product
 */
class Product implements ProductInterface
{
	/**
	 * Product Id.
	 *
	 * @var string
	 */
	private $id;

	/**
	 * Product price.
	 *
	 * @var float
	 */
	private $price;

	/**
	 * Product volume prices.
	 *
	 * @var array
	 */
	private $volumePrices = [];

	/**
	 * Count of specific product items.
	 *
	 * @var int
	 */
	private $productItemsCount;


	/**
	 * Product constructor.
	 *
	 * @param string $id           Product Id.
	 * @param float  $price        Product price.
	 * @param array  $volumePrices Product volume prices.
	 */
	public function __construct(string $id, float $price, array $volumePrices)
	{
		$this->id                = $id;
		$this->price             = $price;
		$this->volumePrices      = $volumePrices;
		$this->productItemsCount = 1;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * Returns product price.
	 *
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * Returns volume price product data.
	 *
	 * @return array
	 */
	public function getVolumePrices(): array
	{
		return $this->volumePrices;
	}

	/**
	 * Adds specific products item to the counter.
	 */
	public function addProductItem()
	{
		$this->productItemsCount++;
	}

	/**
	 * Returns count of specific products.
	 *
	 * @return int
	 */
	public function getProductItemsCount()
	{
		return $this->productItemsCount;
	}

	/**
	 * {@inheritDoc}
	 */
	public function getTotalPrice(): float
	{
		return $this->price * $this->productItemsCount;
	}
}