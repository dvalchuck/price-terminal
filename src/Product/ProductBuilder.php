<?php


namespace PriceTerminal\Product;


use PriceTerminal\Exception\PriceTerminalException;

/**
 * Builder class for product object.
 *
 * @package PriceTerminal\Product
 */
class ProductBuilder
{

	/**
	 * Simple product object.
	 *
	 * @var Product
	 */
	protected $product;

	/**
	 * Products configuration array.
	 *
	 * @var array
	 */
	private $_productConfiguration;

	/**
	 * ProductBuilder constructor.
	 *
	 * @param array $productsConfiguration Products configuration array.
	 */
	public function __construct(array $productsConfiguration)
	{
		$this->_productConfiguration = $productsConfiguration;
	}

	/**
	 * Returns simple product object.
	 *
	 * @return Product
	 */
	public final function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * Build's Product object.
	 *
	 * @param string $productId Product to build.
	 *
	 * @return $this
	 * @throws PriceTerminalException
	 */
	public function buildProduct(string $productId)
	{
		if (!isset($this->_productConfiguration[$productId])) {
			throw new PriceTerminalException('Failed to find ' . $productId . ' product configuration.');
		}
		$price       = $this->_productConfiguration[$productId]['price'];
		$volumePrice = isset($this->_productConfiguration[$productId]['volumePrices'])
			? $this->_productConfiguration[$productId]['volumePrices'] : [];

		$this->product = new Product($productId, $price, $volumePrice);

		return $this;
	}

}