<?php

namespace PriceTerminal\Tests;

use PHPUnit\Framework\TestCase;
use PriceTerminal\Exception\PriceTerminalException;
use PriceTerminal\Terminal;

class PriceTerminalTest extends TestCase
{
	/**
	 * @var Terminal
	 */
	protected $terminal;

	/**
	 * {@inheritDoc}
	 */
	public function setUp()
	{
		$config         = [
			'ZA' => ['price' => 2, 'volumePrices' => [4 => 7]],
			'YB' => ['price' => 12],
			'FC' => ['price' => 1.25, 'volumePrices' => [6 => 6]],
			'GD' => ['price' => 0.15]
		];
		$this->terminal = new Terminal($config);
		parent::setUp();
	}

	/**
	 * Tests first case.
	 *
	 * @return void
	 */
	public function testSuccessFirst()
	{
		try {
			$this->terminal->addProducts(['ZA', 'YB', 'FC', 'GD', 'ZA', 'YB', 'ZA', 'ZA']);
		} catch (PriceTerminalException $exception) {
			self::fail('Filed to parse defined products.');
		}

		$total = $this->terminal->getTotal();

		self::assertEquals(32.40, $total, "Total price was calculated incorrectly.");
	}

	/**
	 * Tests first case.
	 *
	 * @return void
	 */
	public function testSuccessSecond()
	{
		try {
			$this->terminal->addProducts(['FC', 'FC', 'FC', 'FC', 'FC', 'FC', 'FC']);
		} catch (PriceTerminalException $exception) {
			self::fail('Filed to parse defined products.');
		}

		$total = $this->terminal->getTotal();

		self::assertEquals(7.25, $total, "Total price was calculated incorrectly.");
	}

	/**
	 * Tests first case.
	 *
	 * @return void
	 */
	public function testSuccessThird()
	{
		try {
			$this->terminal->addProducts(['ZA', 'YB', 'FC', 'GD']);
		} catch (PriceTerminalException $exception) {
			self::fail('Filed to parse defined products.');
		}

		$total = $this->terminal->getTotal();

		self::assertEquals(15.40, $total, "Total price was calculated incorrectly.");
	}

	/**
	 * Test to check extra volume prices.
	 */
	public function testSuccessExtra()
	{
		$config   = [
			'ZA' => ['price' => 2, 'volumePrices' => [4 => 7, 5 => 8]],
			'YB' => ['price' => 12],
			'FC' => ['price' => 1.25, 'volumePrices' => [6 => 6]],
			'GD' => ['price' => 0.15]
		];
		$terminal = new Terminal($config);

		try {
			$terminal->addProducts(['ZA', 'YB', 'FC', 'GD', 'ZA', 'YB', 'ZA', 'ZA', 'ZA']);
		} catch (PriceTerminalException $exception) {
			self::fail('Filed to parse defined products.');
		}

		$total = $terminal->getTotal();

		self::assertEquals(33.40, $total, "Total price was calculated incorrectly.");
	}

	/**
	 * Tests error case.
	 *
	 * @expectedException \PriceTerminal\Exception\PriceTerminalException
	 * @expectedExceptionMessage Failed to find not-exists product configuration.
	 * @return void
	 */
	public function testErrorNotExistProduct()
	{
		$this->terminal->addProducts(['ZA', 'YB', 'FC', 'not-exists']);

		self::fail('Filed to parse defined products.');
	}

}
